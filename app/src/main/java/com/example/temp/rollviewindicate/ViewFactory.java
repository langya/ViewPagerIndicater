package com.example.temp.rollviewindicate;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

/**
 * Created by weipengjie on 15/12/30.
 */
public class ViewFactory {
    private static Bitmap bmp;

    public static ImageView getImageView(Context context, String imageUrl) {
        ImageView imageView = new ImageView(context);

        ImageLoader.getInstance().loadImage(imageUrl, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {

            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                bmp = bitmap;
            }

            @Override
            public void onLoadingCancelled(String s, View view) {

            }
        });

        imageView.setImageBitmap(bmp);
        return imageView;
    }
}
