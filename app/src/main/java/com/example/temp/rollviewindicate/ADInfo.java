package com.example.temp.rollviewindicate;

/**
 * Created by weipengjie on 15/12/30.
 */
public class ADInfo {

    private String url = "";
    private String content = "";

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
